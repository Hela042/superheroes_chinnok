USE SuperheroesDB

CREATE TABLE SuperheroPowers (
	SuperheroId int NOT NULL FOREIGN KEY REFERENCES Superhero(Id),
	PowerId int NOT NULL FOREIGN KEY REFERENCES Power(Id),
	PRIMARY KEY (SuperheroId, PowerId)
)