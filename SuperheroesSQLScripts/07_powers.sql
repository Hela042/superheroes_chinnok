USE SuperheroesDB

INSERT INTO Power
VALUES ('Flight', 'Soar through the skies')

INSERT INTO Power
VALUES ('Super Strength', 'Strength of ten men')

INSERT INTO power
VALUES ('Telekinesis', 'Move objects with your mind')

INSERT INTO Power
VALUES ('Teleportation', 'Move where you want in an instant')

INSERT INTO SuperheroPowers
VALUES (1,1)

INSERT INTO SuperheroPowers
VALUES (1,2)

INSERT INTO SuperheroPowers
VALUES (2,1)

INSERT INTO SuperheroPowers
VALUES (2,3)

INSERT INTO SuperheroPowers
VALUES (3,2)

INSERT INTO SuperheroPowers
VALUES (3,4)