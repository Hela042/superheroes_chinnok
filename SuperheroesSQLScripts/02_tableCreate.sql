USE SuperheroesDB

CREATE TABLE Superhero (
	Id int PRIMARY KEY Identity (1,1) NOT NULL,
	Name nvarchar(50) NOT NULL,
	Alias nvarchar(50) NOT NULL,
	origin nvarchar(50) NULL
)

CREATE TABLE Assistant (
	Id int PRIMARY KEY identity(1,1) NOT NULL,
	Name nvarchar(50) NOT NULL
)

CREATE TABLE Power (
	Id int PRIMARY KEY identity(1,1) NOT NULL,
	Name nvarchar(50) NOT NULL,
	Description nvarchar(50) NOT NULL
)