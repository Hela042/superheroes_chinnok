# Assignment Create a database and access it

Assignment for Noroff Accelerate winter 2022. Task is to make SQL server scripts to create and populate a superheroes database. And create a C# console application to access a database with SQL Client.

## Description

The first task is to create SQL scripts to create and populate a superheroes database. These can be found int the SuperheroesSQLScripts folder.
The second task is to create a console application that can do simple CRUD functionality and more specific read functions with the help of the SQL Client library.


## Getting Started

### Dependencies

* .Net Framework
* Visual Studio 2019/22 OR Visual Studio Code

### Before Executing
* Remember to set your SQL server data source in the data source field in the ConnectionHelper.cs file

### Executing program
* clone repository / download
* Open solution in Visual Studio
* Build and run

## Author

Jessica Lindqvist [@Jessica.Lindqvist]
Lukas Mårtensson [@Hela042]
