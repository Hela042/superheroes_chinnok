﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chinook_app.Models
{
    public class CustomerGenre
    {
        public List<string> PopularGenres { get; set; } = new List<string>();
    }
}
