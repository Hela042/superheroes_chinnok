﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chinook_app.Models
{
    public class CustomerCountry
    {
        public Dictionary<string, int> CustomerPerCountry { get; set; } = new Dictionary<string, int>();
    }
}
