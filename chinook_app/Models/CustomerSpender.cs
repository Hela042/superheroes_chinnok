﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chinook_app.Models
{
    public class CustomerSpender
    {
        public List<Customer> CustomersSpenders { get; set; } = new List<Customer>();
    }
}
