﻿using chinook_app.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chinook_app.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public bool AddCustomer(Customer customer)
        {
            bool success = false;
            string sqlNew = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" + //The query
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open(); //Open a connection to the sql server
                    using (SqlCommand command = new SqlCommand(sqlNew, connect))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", customer.Email);

                        success = command.ExecuteNonQuery() > 0 ? true : false; //If a row was affected success is true, else false
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message); //If something went wrong, display the error
            }
            return success;
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>(); //Make a list for the customers
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer"; //The info wanted from the database
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer myCustomers = new Customer();
                                myCustomers.CustomerId = reader.GetInt32(0);                              
                                myCustomers.FirstName = reader.GetString(1);
                                myCustomers.LastName = reader.GetString(2);
                                myCustomers.Country = reader.GetString(3);
                                myCustomers.PostalCode = reader.IsDBNull(4)? null : reader.GetString(4); //If field is NULL
                                myCustomers.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5); //If field is NULL
                                myCustomers.Email = reader.GetString(6);

                                customerList.Add(myCustomers); //Add everything to the list
                            }
                        }
                    }
                }
            } catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        public Customer GetCustomer(int ID)
        {
            Customer customer = new Customer();
            string OneSql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerID = @CustomerID";
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(OneSql, connect))
                    {
                        command.Parameters.AddWithValue("@CustomerID", ID);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public Customer GetCustomer(string name, string lastName)
        {
            Customer customer = new Customer();
            string OneSql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName = @FirstName AND LastName = @LastName";
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(OneSql, connect))
                    {
                        command.Parameters.AddWithValue("@FirstName", name);
                        command.Parameters.AddWithValue("@LastName", lastName);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public IEnumerable<Customer> GetLimitedCustomers(int limit, int offset)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerID BETWEEN @1 AND @9";
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {
                        command.Parameters.AddWithValue("@1", limit);
                        command.Parameters.AddWithValue("@9", offset);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer myCustomers = new Customer();
                                myCustomers.CustomerId = reader.GetInt32(0);
                                myCustomers.FirstName = reader.GetString(1);
                                myCustomers.LastName = reader.GetString(2);
                                myCustomers.Country = reader.GetString(3);
                                myCustomers.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                myCustomers.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                myCustomers.Email = reader.GetString(6);

                                customerList.Add(myCustomers);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer " + 
                "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @PhoneNumber, Email = @Email " +
                "WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }

        public CustomerCountry GetCustomerCountry()
        {
            CustomerCountry customerCountry = new CustomerCountry();
            string sql = "SELECT Country, COUNT(CustomerId) AS Count FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY COUNT(CustomerId) DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customerCountry.CustomerPerCountry.Add(reader.GetString(0), reader.GetInt32(1));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerCountry;
        }

        public CustomerSpender GetHighestSpenders(int limit)
        {
            CustomerSpender customerSpender = new CustomerSpender();
            string sql = "SELECT TOP (@limit) Customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "INNER JOIN Invoice " +
                "ON Customer.CustomerId = Invoice.CustomerId " +
                "GROUP BY Customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "ORDER BY SUM(Invoice.Total) DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "" : reader.GetString(4);
                                temp.PhoneNumber = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerSpender.CustomersSpenders.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerSpender;
        }

        public CustomerGenre GetMostPopularGenre(int id)
        {
            CustomerGenre customerGenre = new CustomerGenre();
            string sql = "SELECT TOP(1) WITH TIES Genre.Name, COUNT(Genre.GenreId) AS GenreCount " +
                "FROM Genre " +
                "INNER JOIN Track " +
                "ON Genre.GenreId = Track.GenreId " +
                "INNER JOIN InvoiceLine " +
                "ON Track.TrackId = InvoiceLine.TrackId " +
                "INNER JOIN Invoice " +
                "ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "INNER JOIN Customer " +
                "ON Invoice.CustomerId = Customer.CustomerId " +
                "WHERE Customer.CustomerId = @id " +
                "GROUP BY Genre.Name " +
                "ORDER BY GenreCount DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customerGenre.PopularGenres.Add(reader.GetString(0));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerGenre;
        }
    }
}
