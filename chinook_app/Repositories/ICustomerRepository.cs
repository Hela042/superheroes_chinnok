﻿using chinook_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chinook_app.Repositories
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Fetches all customers from the database and inserts the customer objects into an enumerable of Customers.
        /// </summary>
        /// <returns>An enumerable of Customers.</returns>
        public IEnumerable<Customer> GetAllCustomers();
        /// <summary>
        /// Fetches the customer that has the passed in Id.
        /// </summary>
        /// <param name="id">Id of the customer to fetch.</param>
        /// <returns>A customer object.</returns>
        public Customer GetCustomer(int id);
        /// <summary>
        /// Fetches the customer that has the passed in firstname and lastname .
        /// </summary>
        /// <param name="name">The customers first name</param>
        /// <param name="lastName">The customers last name</param>
        /// <returns>A customer object.</returns>
        public Customer GetCustomer(string name, string lastName);
        /// <summary>
        /// Fetches customers based on the passed in limit and offset.
        /// </summary>
        /// <param name="limit">Limit of customers to fetch.</param>
        /// <param name="offset">Offset of where to start fetching customers</param>
        /// <returns>An enumerable of Customers</returns>
        public IEnumerable<Customer> GetLimitedCustomers(int limit, int offset);
        /// <summary>
        /// Add a new customer to the database.
        /// </summary>
        /// <param name="customer">Customer object to add.</param>
        /// <returns>true if the customer was successfully added, false if it was a failure.</returns>
        public bool AddCustomer(Customer customer);
        /// <summary>
        /// Updates the passed in customer with the same id as the customer objects customerId with the rest of the customer objects properties.
        /// </summary>
        /// <param name="customer">Updated customer</param>
        /// <returns>true if the update was successful and false if the updated resulted in a failure. </returns>
        public bool UpdateCustomer(Customer customer);
        /// <summary>
        /// Fetches the number of customers per country and stores the Country name and customer count in a dictionary that is stored in a CustomerCountry object.
        /// </summary>
        /// <returns>A CustomerCountry object which contains a dictionary with the fetched information.</returns>
        public CustomerCountry GetCustomerCountry();
        /// <summary>
        /// Fetches the highest spenders and stores them in a Customer list in a CustomerSpender onject.
        /// </summary>
        /// <param name="limit">number of customers to fetch</param>
        /// <returns>A customerSpender object containing a list of the customers spending the most.</returns>
        public CustomerSpender GetHighestSpenders(int limit);
        /// <summary>
        /// Fetches the specified customers most popular genre, will fetch more than one genre incase of a tie. Stores the genre in a list contained in a CustomerGenre object. 
        /// </summary>
        /// <param name="id">CustomerId of the customer</param>
        /// <returns>A CustomerGenre object that contains a list the customers favorite genre.</returns>
        public CustomerGenre GetMostPopularGenre(int id);
    }
}
