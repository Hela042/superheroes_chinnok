﻿using chinook_app.Models;
using chinook_app.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chinook_app
{
    public class CustomerTestUI
    {
        private ICustomerRepository repository;

        public CustomerTestUI(ICustomerRepository repository)
        {
            this.repository = repository;
        }
        /// <summary>
        /// Calls printCustomer for every customer in the customer enumerable.
        /// </summary>
        /// <param name="customers">An enumerable of type Customer</param>
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        /// <summary>
        /// Prints out to the console the passed in customer object.
        /// </summary>
        /// <param name="customer">A customer object</param>
        static void PrintCustomer(Customer customer) //What should be displayd
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.PhoneNumber} {customer.Email} ---");
        }
        /// <summary>
        /// Does a test on the getAllcustomers method and calls printcustomers on the result.
        /// </summary>
        public void TestGetCustomers()
        {
            PrintCustomers(repository.GetAllCustomers());
        }
        /// <summary>
        /// Does a test on the Getcustomer method by id and prints the result to the console.
        /// </summary>
        public void TestGetCustomerById()
        {
            PrintCustomer(repository.GetCustomer(2));
        }
        /// <summary>
        /// Does a test on the Getcustomer method by name and prints the result to the console.
        /// </summary>
        public void TestGetCustomerByName()
        {
            PrintCustomer(repository.GetCustomer("Bjørn", "Hansen"));
        }
        /// <summary>
        /// Does a test on the getLimitedCustomers method with hardcoded parameters and prints out the result.
        /// </summary>
        public void TestGetLimitedCustomer()
        {
            PrintCustomers(repository.GetLimitedCustomers(1, 9));
        }
        /// <summary>
        /// Does a test on the AddCustomer method with a hardcoded Customer object, and prints out the added customer if successfull.
        /// </summary>
        public void TestAddCustomer()
        {
            Customer NewCustomer = new Customer() //Add the customer (Dont add the CustomerID it is auto incremented)
            {
                FirstName = "Wilbur",
                LastName = "Watson",
                Country = "England",
                PostalCode = "postCode",
                PhoneNumber = "+99 555099044",
                Email = "WilburWatson@MadeUpEmail.wow"

            };
            if (repository.AddCustomer(NewCustomer)) //If it was a success
            {
                Console.WriteLine("Added customer! :D");
                PrintCustomer(repository.GetCustomer("Wilbur", "Watson")); //Display the new customer
            }
            else
            {
                Console.WriteLine("Did not add customer D:"); //If something went wrong in adding the customer
            }
        }
        /// <summary>
        /// Does a test on the UpdateCustomer method.
        /// </summary>
        public void TestUpdateCustomer()
        {
            Customer test = new Customer()
            {
                CustomerId = 7,
                FirstName = "Astrid",
                LastName = "Goober",
                Country = "Austria",
                PostalCode = "1010",
                PhoneNumber = "+43 01 5134505",
                Email = "astrid.gruber@apple.at"
            };
            if (repository.UpdateCustomer(test))
            {
                Console.WriteLine("Success");
                PrintCustomer(repository.GetCustomer(7));
            }
            else
            {
                Console.WriteLine("Failiure");
            }
        }
        /// <summary>
        /// Does a test on the GetCustomerCountry method and prints the result to the console.
        /// </summary>
        public void TestGetCustomerCountry()
        {
            CustomerCountry customerCountry = repository.GetCustomerCountry();
            foreach (KeyValuePair<string, int> item in customerCountry.CustomerPerCountry)
            {
                Console.WriteLine($"Country: {item.Key} Count: {item.Value}");
            }
        }
        /// <summary>
        /// Does a test on the getHighestSpenders method and prints the result to the console.
        /// </summary>
        public void TestHighestSpenders()
        {
            CustomerSpender customerSpender = repository.GetHighestSpenders(5);
            foreach (var item in customerSpender.CustomersSpenders)
            {
                Console.WriteLine($"{item.FirstName} {item.LastName}");
            }
        }
        /// <summary>
        /// Does a test on the getMostPopularGenre method with a hardcoded CustomerId (5) and prints the result to the console.
        /// </summary>
        public void TestGetMostPopularGenre()
        {
            CustomerGenre customerGenre = repository.GetMostPopularGenre(5);
            foreach (var genre in customerGenre.PopularGenres)
            {
                Console.WriteLine(genre);
            }
        }
    }
}
