﻿using chinook_app.Models;
using chinook_app.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace chinook_app
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            CustomerTestUI testUI = new CustomerTestUI(repository);
            // A customerTestUI has been instanciated, The UI class contains test Methods for all functionality that we implemented.
            //testUI.TestGetCustomers();
            //testUI.TestGetCustomerById();
            //testUI.TestGetCustomerByName();
            //testUI.TestGetLimitedCustomer();
            //testUI.TestAddCustomer();
            //testUI.TestUpdateCustomer();
            //testUI.TestGetCustomerCountry();
            //testUI.TestHighestSpenders();
            //testUI.TestGetMostPopularGenre();
        }
    }
}
